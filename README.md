# APC Propeller Performance Data Processor

Python 3 build by Lu�s Parada for personal use. It aims to assist the analysis 
of APC propeller performance files available in https://www.apcprop.com/technical-information/performance-data/. 

 
 
## Requirements <a name="requirements"></a>
- **scipy.interpolate**


## Files <a name="files"></a>

- **apc_prop_processor.py** - Source code file that shall be run to begin software execution. 

- **PER3_125x9.dat** - Performance data file example of a 12.5 x 9 propeller.

In order to run the program the user must change the propeller file path set in **apc_prop_processer.py**.


### Global Methods <a name="global_methods"></a>

- **init_lists**() - Initializes lists that store temporarily different sets of data.

- **read_file_data**(`string` path) - reads and stores file data in organized lists.

- **data_interpolator**(`string` path, `float` airspeed, `float` thrust) - For a given airspeed and desired 
output thrust, interpolates several performace variables of the propeller to which the file path refers to.











