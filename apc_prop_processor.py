import scipy.interpolate as si


def init_lists():
    rpm = 0
    v = []
    j_rat = []
    ct = []
    cp = []
    pwr = []
    torque = []
    thrust = []
    pe = []

    return{'rpm': rpm, 'v': v, 'j_rat': j_rat, 'ct': ct, 'cp': cp, 'pwr': pwr, 'torque': torque,
           'thrust': thrust, 'pe': pe}


def read_file_data(path):

    #   APC file nomenclature

    #   rpm     - Rotations per minute
    #   v       - Airspeed in mph
    #   j_rat   - Advance ratio = v/nD
    #   n       - number of blades
    #   D       - propeller diameter
    #   ct      - thrust coefficient ct = thrust/(rho * n**2 * D**4)
    #   cp      - power coefficient cp = power/(rho * n**3 * D**5)
    #   rho     - air density
    #   torque in In-Lbf
    #   thrust in Lbf
    #   pwr     - shaft power in Hp
    #   pe      - propeller efficiency

    file_data = []
    rpm_data = init_lists()

    try:
        with open(path, mode='rt') as input_file:
            input_file.readline()

            for line in input_file:
                if 'RPM' in line:
                    if rpm_data['rpm'] != 0:
                        file_data.append(rpm_data)
                        rpm_data = init_lists()

                    split_line = line.split(' ')
                    for elem in split_line:
                        try:
                            rpm_data['rpm'] = float(elem)
                            break
                        except ValueError:
                            continue

                else:
                    split_line = line.split('   ')
                    aux = []
                    for var in split_line:
                        try:
                            var.replace(' ', '')
                            aux.append(float(var))
                        except ValueError:
                            continue
                        if len(aux) == 8:
                            rpm_data['v'].append(aux[0])
                            rpm_data['j_rat'].append(aux[1])
                            rpm_data['pe'].append(aux[2])
                            rpm_data['ct'].append(aux[3])
                            rpm_data['cp'].append(aux[4])
                            rpm_data['pwr'].append(aux[5])
                            rpm_data['torque'].append(aux[6])
                            rpm_data['thrust'].append(aux[7])
                            break

    except FileNotFoundError:
        print('No file found')

    file_data.append(rpm_data)

    return file_data


def data_interpolator(path, airspeed, thrust):
    v_mph = airspeed / 0.44704  # convert to mph
    T_lbf = thrust * 0.224808943871  # convert to Lbf

    prop_file_data = read_file_data(path)

    rpm_v = []
    j_rat_v = []
    ct_v = []
    cp_v = []
    pwr_v = []
    torque_v = []
    thurst_v = []
    pe_v = []

    for i in range(len(prop_file_data)):

        rpm_v.append(prop_file_data[i]['rpm'])
        j_rat_vs_v = si.interp1d(prop_file_data[i]['v'], prop_file_data[i]['j_rat'], kind='cubic',
                                 fill_value='interpolate')
        ct_vs_v = si.interp1d(prop_file_data[i]['v'], prop_file_data[i]['ct'], kind='cubic',
                              fill_value='interpolate')
        cp_vs_v = si.interp1d(prop_file_data[i]['v'], prop_file_data[i]['cp'], kind='cubic',
                              fill_value='interpolate')
        pwr_vs_v = si.interp1d(prop_file_data[i]['v'], prop_file_data[i]['pwr'], kind='cubic',
                               fill_value='interpolate')
        torque_vs_v = si.interp1d(prop_file_data[i]['v'], prop_file_data[i]['torque'], kind='cubic',
                                  fill_value='interpolate')
        thrust_vs_v = si.interp1d(prop_file_data[i]['v'], prop_file_data[i]['thrust'], kind='cubic',
                                  fill_value='interpolate')
        pe_vs_v = si.interp1d(prop_file_data[i]['v'], prop_file_data[i]['pe'], kind='cubic',
                              fill_value='interpolate')

        try:
            j_rat_v.append(float(j_rat_vs_v(v_mph)))
            ct_v.append(float(ct_vs_v(v_mph)))
            cp_v.append(float(cp_vs_v(v_mph)))
            pwr_v.append(float(pwr_vs_v(v_mph)))
            torque_v.append(float(torque_vs_v(v_mph)))
            thurst_v.append(float(thrust_vs_v(v_mph)))
            pe_v.append(float(pe_vs_v(v_mph)))

        except ValueError:
            rpm_v = rpm_v[:-1]
            continue

    rpm_v_vs_thrust = si.interp1d(thurst_v, rpm_v, kind='cubic', fill_value='interpolate')

    j_rat_v_vs_thrust = si.interp1d(thurst_v, j_rat_v, kind='cubic', fill_value='interpolate')

    ct_v_vs_thrust = si.interp1d(thurst_v, ct_v, kind='cubic', fill_value='interpolate')

    cp_v_vs_thrust = si.interp1d(thurst_v, cp_v, kind='cubic', fill_value='interpolate')

    pwr_v_vs_thrust = si.interp1d(thurst_v, pwr_v, kind='cubic', fill_value='interpolate')

    torque_v_vs_thrust = si.interp1d(thurst_v, torque_v, kind='cubic', fill_value='interpolate')

    pe_v_vs_thrust = si.interp1d(thurst_v, pe_v, kind='cubic', fill_value='interpolate')

    rpm = rpm_v_vs_thrust(T_lbf)
    j_rat = j_rat_v_vs_thrust(T_lbf)
    ct = ct_v_vs_thrust(T_lbf)
    cp = cp_v_vs_thrust(T_lbf)
    pwr = pwr_v_vs_thrust(T_lbf)
    torque = torque_v_vs_thrust(T_lbf)
    pe = pe_v_vs_thrust(T_lbf)

    return {'rpm': rpm, 'j_rat': j_rat, 'ct': ct, 'cp': cp, 'pwr': pwr, 'torque': torque, 'pe': pe}


# Inputs ----------------------------------------------------------------------------------------

file_path = 'C:\\apc_prop_tool\\PER3_125x9.dat'

v = 17      # [m/s]
Th = 3.19   # Thrust [N]
# -----------------------------------------------------------------------------------------------

print('\nInput:')
print('Airspeed: %f m/s' % v)
print('Thrust: %f N' % Th)
# -----------------------------------------------------------------------------------------------

propeller_data = data_interpolator(file_path, v, Th)
print('\nOutput:')
print('Propeller at %f RPM' % propeller_data['rpm'])
print('Advance Ratio J : %f' % propeller_data['j_rat'])
print('Thrust Coefficient Ct : %f' % propeller_data['ct'])
print('Power Coefficient Cp : %f' % propeller_data['cp'])
print('Shaft Power: %f Hp' % propeller_data['pwr'])
print('Applied Torque: %f In-Lbf' % propeller_data['torque'])
print('Efficiency: %f' % propeller_data['pe'])
